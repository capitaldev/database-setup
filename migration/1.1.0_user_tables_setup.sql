CREATE TABLE `capital`.`user_role` (
    `user_role_id` INT NOT NULL AUTO_INCREMENT,
    `user_role_name` VARCHAR(100) NOT NULL,
    PRIMARY KEY (`user_role_id`),
    UNIQUE INDEX `user_role_name_UNIQUE` (`user_role_name` ASC) VISIBLE
);

CREATE TABLE `capital`.`user_permission` (
    `permission_id` INT NOT NULL AUTO_INCREMENT,
    `permission_name` VARCHAR(100) NOT NULL,
    PRIMARY KEY (`permission_id`),
    UNIQUE INDEX `permission_name_UNIQUE` (`permission_name` ASC) VISIBLE
);

CREATE TABLE `capital`.`user_type` (
    `user_type_id` INT NOT NULL AUTO_INCREMENT,
    `user_type_name` VARCHAR(100) NOT NULL,
    PRIMARY KEY (`user_type_id`),
    UNIQUE INDEX `user_type_name_UNIQUE` (`user_type_name` ASC) VISIBLE
);

CREATE TABLE `capital`.`user_role_permission_map` (
    `role_permission_map_id` INT NOT NULL AUTO_INCREMENT,
    `user_role_id` INT NOT NULL,
    `permission_id` INT NOT NULL,
    PRIMARY KEY (`role_permission_map_id`),
    UNIQUE INDEX `role_permission_map_id_UNIQUE` (`role_permission_map_id` ASC) VISIBLE,
    INDEX `user_role_id_idx` (`user_role_id` ASC) VISIBLE,
    INDEX `permission_id_idx` (`permission_id` ASC) VISIBLE,
    CONSTRAINT `user_role_id` FOREIGN KEY (`user_role_id`) REFERENCES `capital`.`user_role` (`user_role_id`),
    CONSTRAINT `permission_id` FOREIGN KEY (`permission_id`) REFERENCES `capital`.`user_permission` (`permission_id`)
);

CREATE TABLE `capital`.`user` (
    `user_id` INT NOT NULL AUTO_INCREMENT,
    `user_role_id` INT NOT NULL,
    `user_type_id` INT NOT NULL,
    `created_by` INT NOT NULL,
    `last_updated_by` INT NOT NULL,
    `is_active` TINYINT NOT NULL,
    `custom_permissions` JSON,
    `created_timestamp` TIMESTAMP NOT NULL,
    `updated_timestamp` TIMESTAMP NOT NULL,
    PRIMARY KEY (`user_id`),
    UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC) VISIBLE,
    INDEX `user_role_id_user_idx` (`user_role_id` ASC) VISIBLE,
    INDEX `user_type_id_user_idx` (`user_type_id` ASC) VISIBLE,
    INDEX `created_by_user_idx` (`created_by` ASC) VISIBLE,
    INDEX `last_updated_by_user_idx` (`last_updated_by` ASC) VISIBLE,
    CONSTRAINT `user_role_id_user_idx` FOREIGN KEY (`user_role_id`) REFERENCES `capital`.`user_role` (`user_role_id`),
    CONSTRAINT `user_type_id` FOREIGN KEY (`user_type_id`) REFERENCES `capital`.`user_type` (`user_type_id`),
    CONSTRAINT `created_by` FOREIGN KEY (`created_by`) REFERENCES `capital`.`user` (`user_id`),
    CONSTRAINT `last_updated_by` FOREIGN KEY (`last_updated_by`) REFERENCES `capital`.`user` (`user_id`)
);

CREATE TABLE `capital`.`user_detail` (
    `user_id` INT NOT NULL,
    `email` VARCHAR(100),
    `phone` VARCHAR(100),
    `adress` VARCHAR(1000),
    `pin_code` VARCHAR(100),
    PRIMARY KEY (`user_id`),
    INDEX `user_id_user_detail_idx` (`user_id` ASC) VISIBLE,
    CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `capital`.`user` (`user_id`)
);